module ApplicationHelper
    def flash_class(level)
        case level
            when "notice" then "alert-primary"
            when "success" then "alert-success"
            when "error" then "alert-danger"
            else "alert-warning"
        end
    end
end
