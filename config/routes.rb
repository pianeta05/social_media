Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }


  as :user do
    get 'sign-up', to: 'devise/registrations#new', as: :sign_up
    get 'sign-in', to: 'devise/sessions#new', as: :sign_in
    delete 'sign_out', to: 'devise/sessions#destroy', as: :sign_out
    get 'profile', to: 'devise/registrations#edit', as: :profile
    put 'users/:user_id/admin', to: 'users/registrations#admin', as: :admin
    put 'users/:user_id/unadmin', to: 'users/registrations#unadmin', as: :unadmin
  end
  
  root 'pages#home'
end
