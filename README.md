# Proyecto Devise

Proyecto de práctica para las clases de Rails. Para descargarlo:

```bash
git clone git@bitbucket.org:agcastro7/devise-project.git
cd devise-project
bundle
yarn install
rails db:migrate
rails s
```
