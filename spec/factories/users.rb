FactoryBot.define do
  factory :user do
    email                 { Faker::Internet.unique.email }
    name                  { Faker::Name.name }
    description           { Faker::Lorem.sentence(word_count: 10) }
    image                 { Faker::LoremPixel.image(size: "150x150") }
    password              { "password" }
    password_confirmation { "password" }
  end
end
